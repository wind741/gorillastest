package com.example.testapp.Activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.example.testapp.Fragments.FragmentProducts;
import com.example.testapp.R;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameLayout ;
    Fragment currentFragment;
    FragmentManager fragmentManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("Welcome");

        frameLayout = findViewById(R.id.main_frame);
        fragmentManager = getSupportFragmentManager();
        currentFragment = new FragmentProducts();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_frame, currentFragment);
        transaction.commit();
    }
}
