package com.example.testapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testapp.Models.ProductModel;
import com.example.testapp.R;

import java.util.ArrayList;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> implements Filterable {

    private ArrayList<ProductModel> products;
    private Context context;
    private TextView productName;
    private TextView productPrice;
    private ImageView productImage;


    public ProductAdapter(ArrayList<ProductModel> products) {
        this.products = products;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            productName= itemView.findViewById(R.id.tv_product_name);
            productPrice = itemView.findViewById(R.id.tv_product_price);
            productImage = itemView.findViewById(R.id.img_cone_type);
        }

        @Override
        public void onClick(View v) {

        }
    }
    @Override
    public Filter getFilter() {
        return null;
    }

    @NonNull
    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_products,parent,false);
        return new ProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.MyViewHolder holder, int position) {
        productName.setText(products.get(position).getName1() + " " + products.get(position).getName2() );
        productPrice.setText(products.get(position).getPrice());
        productImage.setBackgroundColor(Color.parseColor(products.get(position).getBg_color()));


        switch (products.get(position).getType()){
            case "cone":
                productImage.setImageResource(R.drawable.cone);
                break;
            case "froyo":
                productImage.setImageResource(R.drawable.froyo);
                break;
            case "popsicle":
                productImage.setImageResource(R.drawable.popsicle);
                break;
            case "ice_cream":
                productImage.setImageResource(R.drawable.ice_cream);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return products.size();
    }


}
