package com.example.testapp.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.testapp.Adapter.ProductAdapter;
import com.example.testapp.Models.ProductModel;
import com.example.testapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentProducts extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String URL_PRODUCTS = "https://gl-endpoint.herokuapp.com/products";
    private RecyclerView recyclerView;
    private ProgressBar pgbProducts;

    private String mParam1;
    private String mParam2;
    private RequestQueue volleyQueue;

    public FragmentProducts() {
    }

    public static FragmentProducts newInstance(String param1, String param2) {
        FragmentProducts fragment = new FragmentProducts();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        pgbProducts = view.findViewById(R.id.pgb_icecream);
        recyclerView = view.findViewById(R.id.rv_products);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        getProducts();
        return view;
    }

    private void getProducts() {
        // volley implementation from https://developer.android.com/training/volley
        volleyQueue = Volley.newRequestQueue(getContext());

        pgbProducts.setVisibility(View.VISIBLE);
        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, URL_PRODUCTS, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<ProductModel> products = new ArrayList<>();
                        for (int currentProduct = 0; currentProduct < response.length(); currentProduct++) {
                            ProductModel productModel = new ProductModel();
                            JSONObject currentProductModel = null;
                            try {
                                currentProductModel = response.getJSONObject(currentProduct);

                                productModel.setName1(currentProductModel.getString("name1"));
                                productModel.setName2(currentProductModel.getString("name2"));
                                productModel.setPrice(currentProductModel.getString("price"));
                                productModel.setBg_color(currentProductModel.getString("bg_color"));
                                productModel.setType(currentProductModel.getString("type"));
                                products.add(productModel);
                                pgbProducts.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pgbProducts.setVisibility(View.GONE);
                            }

                        }
                        ProductAdapter productAdapter = new ProductAdapter(products);
                        recyclerView.setAdapter(productAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println(error.getNetworkTimeMs());
                    }
                });
        volleyQueue.add(jsObjRequest);
    }

}
